import logging
import sys

DB_HOST = 'localhost'
DB_NAME = 'wiki_parser'
DB_USER = 'postgres'
DB_PASSWORD = 'password'

DB_DSN = 'dbname={dbname} user={dbuser} password={dbpassword} host={db_host}'\
    .format(dbname=DB_NAME, dbuser=DB_USER, dbpassword=DB_PASSWORD, db_host=DB_HOST)

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger('wiki-parser')
