CREATE TABLE IF NOT EXISTS pages (
  id SERIAL NOT NULL PRIMARY KEY ,
  url TEXT NOT NULL,
  request_depth INTEGER NOT NULL
);

CREATE TABLE IF not EXISTS page_relation (
  id SERIAL NOT NULL PRIMARY KEY,
  from_page_id INTEGER NOT NULL,
  link_id INTEGER NOT NULL,

  FOREIGN KEY (from_page_id) REFERENCES pages(id),
  FOREIGN KEY (link_id) REFERENCES pages(id)
);