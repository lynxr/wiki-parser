import asyncio

import aiopg


class DbConnector:
    def __init__(self, dsn) -> None:
        self.pool = asyncio.get_event_loop().run_until_complete(aiopg.create_pool(dsn, maxsize=2))

    async def execute(self, sql, params=None):
        """
        Искапсуляция aiopg для более легкого вызова метода execute
        :param sql: SQL запрос
        :param params: параметры SQL запроса
        :return: None если нечего возвращать (SELECT) или list с результатом
        """
        async with self.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql, params)
                ret = []
                try:
                    async for row in cur:
                        ret.append(row)
                    return ret
                except Exception:
                    pass
