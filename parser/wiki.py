import asyncio
import re

import aiohttp

from configure.settings import logger
from db.connector import DbConnector


class WikiParser:
    """
    Рекурсивный парсер ссылок из википедии
    """
    SQL_PAGES = """INSERT INTO pages (url, request_depth) VALUES (%s, %s) RETURNING id"""
    SQL_FROM_PAGE_ID = """INSERT INTO page_relation (from_page_id, link_id) VALUES (%s, %s)"""


    # TODO Не знаю можно ли использовать BS4, поэтому regex
    href_regex = re.compile('<a href=[\'"]?([^\'" >]+)')
    base_url = 'http'

    def __init__(self, db_connector: DbConnector, depth=4) -> None:
        self.depth = depth
        self.db_connector = db_connector

    async def _get_page_text(self, page):
        """
        Получение HTML текста страницы
        :param page: URL страницы
        :return: HTML текст, URL
        """
        async with aiohttp.ClientSession() as session:
            async with session.get(page) as response:
                page_text = await response.text()
                url = response.url
                return page_text, url

    def find_links(self, page_text):
        """
        поиск всех ссылок <a href=... на странице
        :param page_text: html страницы
        :return: list ссылок
        """
        links = self.href_regex.findall(page_text)
        return links

    async def parse(self, page):
        await self._parse_page(page)

    def chunks(self, l, n):
        """
        Делитель листа l на чанки длинной n
        :param l: list для разделения
        :param n: размер чанка
        :return:
        """
        # For item i in a range that is a length of l,
        for i in range(0, len(l), n):
            # Create an index range for l of n items:
            yield l[i:i + n]

    async def _parse_page(self, page, cur_depth=0, from_page_id=None):
        """
        Метод парсинга ссылки
        Сохраняет страницу в БД, находит все ссылки, делит их на части и рекурсивно запускает парсинг
        :param page: страница для парсинга
        :param cur_depth: текущая глубина парсинга
        :param from_page_id: id родительской страницы в БД
        :return:
        """
        if cur_depth > self.depth:
            return
        logger.debug('------------------------------------')
        logger.debug('Парсим страницу %s', page)
        try:
            page_text, url = await self._get_page_text(page)
        except:
            logger.error('Не можем распарсить страницу %s', page)
            return
        async with self.db_connector.pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('BEGIN')
                await cur.execute(self.SQL_PAGES, (str(url), cur_depth))
                db_response = await cur.fetchall()
                page_id = db_response[0][0]

                if from_page_id:
                    await cur.execute(self.SQL_FROM_PAGE_ID, (from_page_id, page_id))

                await cur.execute('COMMIT')

        # URL'ы все относительные за редким исключением, приходится формировать абсолютный url
        links = ['https://{}{}'.format(url.host, x) for x in self.find_links(page_text)]
        parse_coors = [self._parse_page(x, cur_depth+1, page_id) for x in links]

        for coors in self.chunks(parse_coors, 20):
            #FIXME У меня просто ноут начал умирать - можно и без чанков если мощей хватит
            await asyncio.wait(coors)
