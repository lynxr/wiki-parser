from asynctest import TestCase

from db.connector import DbConnector
from parser.wiki import WikiParser


class WikiTest(TestCase):
    use_default_loop = True
    forbid_get_event_loop = False

    def setUp(self):
        DB_HOST = 'localhost'
        DB_NAME = 'wiki_parser'
        DB_USER = 'postgres'
        DB_PASSWORD = 'password'

        DB_DSN = 'dbname={dbname} user={dbuser} password={dbpassword} host={db_host}' \
            .format(dbname=DB_NAME, dbuser=DB_USER, dbpassword=DB_PASSWORD, db_host=DB_HOST)

        self.db_connector = DbConnector(DB_DSN)
        self.loop.run_until_complete(self.db_connector.execute('delete from page_relation'))
        self.loop.run_until_complete(self.db_connector.execute('delete from pages'))

    async def test_parse_page(self):
        page = 'https://ya.ru/'

        parser = WikiParser(self.db_connector, depth=1)
        await parser.parse(page)

        resp = await self.db_connector.execute('select * from pages where url = %s', (page,))
        self.assertEqual(len(resp), 1)
        main_page_id, main_page_url, depth = resp[0]

        resp = await self.db_connector.execute('select * from page_relation')

        for row in resp:
            rel_id, from_page_id, link_id = row
            self.assertEqual(from_page_id, main_page_id)
            resp = await self.db_connector.execute('select * from pages where id = %s', (link_id,))
            page_id, page_url, page_depth = resp[0]
            self.assertEqual(page_depth, 1)
