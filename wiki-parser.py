import asyncio
from argparse import ArgumentParser

from configure.settings import logger, DB_DSN
from db.connector import DbConnector
from parser.wiki import WikiParser

if __name__ == '__main__':

    logger.info('Подключаемся к БД')
    db_connector = DbConnector(DB_DSN)

    args_parser = ArgumentParser()
    args_parser.add_argument('--url', required=True)
    args_parser.add_argument('--max-depth', required=True, type=int)

    args = args_parser.parse_args()

    wiki_parser = WikiParser(db_connector, args.max_depth)
    asyncio.get_event_loop().run_until_complete(wiki_parser.parse(args.url))